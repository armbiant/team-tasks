const fs = require("fs");
const { GitLabAPI, GitLabAPIIterator } = require("gitlab-api-async-iterator")();
const { DateTime } = require("luxon");
const {
  CURRENT_PROJECT,
  GROUPS,
  DEFAULT_ISSUE_LABELS,
  IS_DEV,
} = require("./constants");

function UTCDateTimeFromISO(date) {
  return DateTime.fromISO(date, {
    zone: "utc",
  });
}

function getDateOneMonthFromNow() {
  const now = DateTime.utc();
  const nextMonth = now.plus({ months: 1 }).startOf("day");
  return nextMonth;
}

async function getMilestoneForDate(date) {
  console.log(`Getting milestone for ${date.toString()}`);

  const milestones = new GitLabAPIIterator(
    `/projects/${CURRENT_PROJECT}/milestones`,
    { include_parent_milestones: true, state: "active", sort: "desc" }
  );

  for await (const milestone of milestones) {
    const { start_date, due_date, title } = milestone;
    const start = UTCDateTimeFromISO(start_date).startOf("day");
    const due = UTCDateTimeFromISO(due_date).endOf("day");

    console.log(
      `Checking: ${title} which is from ${start.toString()} to ${due.toString()}`
    );

    if (/^\d\d\.\d+$/.test(title) && date >= start && date <= due) {
      console.log(`✅  Next milestone is ${title}.`);
      return milestone;
    }
  }
}

function getPlanningIssueTitle(group, milestone) {
  return `Manage:${group.name} ${milestone.title} Planning issue`;
}

function getAssignQuickActionText(group) {
  return `/assign ${group.assignees.join(" ")}`;
}

function getPlanningIssueDescription(group, milestone) {
  const descriptionTemplate = fs
    .readFileSync(group.planningIssueTemplatePath, "utf8")
    .replace(/00\.0/g, milestone.title);

  const description = `${descriptionTemplate}\n${getAssignQuickActionText(
    group
  )}`;
  return description;
}

async function getIssuesForMilestone(milestone) {
  const { data: issues } = await GitLabAPI(
    `/projects/${CURRENT_PROJECT}/issues`,
    { params: { milestone: milestone.title, labels: "Planning Issue" } }
  );

  return issues;
}

/**
 * Determines whether the given @group needs a planning issue for the [milestone].
 */
function shouldCreatePlanningIssues(existingIssues, group, milestone) {
  console.log(
    `Next Milestone is ${milestone.title}, let's check whether there is a planning issue for ${group.name}`
  );

  // try to find an existing issue that has the same title as the candidate planning issue
  const existingPlanningIssue = existingIssues.find(
    (issue) => issue.title === getPlanningIssueTitle(group, milestone)
  );

  if (existingPlanningIssue) {
    console.log(
      `Seems like ${group.name} has a planning issue for ${milestone.title} already: ${existingPlanningIssue.web_url}`
    );

    return false;
  }

  return true;
}

async function createPlanningIssueForGroup(group, milestone) {
  console.log(
    `Trying to create the issue for ${milestone.title} for ${group.name}`
  );

  try {
    const payload = {
      title: getPlanningIssueTitle(group, milestone),
      milestone_id: milestone.id,
      due_date: milestone.start_date,
      labels: [...DEFAULT_ISSUE_LABELS, ...group.planningIssueLabels].join(","),
      description: getPlanningIssueDescription(group, milestone),
    };

    if (IS_DEV) {
      console.log(
        "[dev] skipped planning issue creation. Payload:",
        JSON.stringify(payload)
      );
      return;
    } else {
      const { data: created } = await GitLabAPI.post(
        `/projects/${CURRENT_PROJECT}/issues`,
        payload
      );
      console.log(
        `✅ Successfully created planning issue for ${group.name}: ${created.web_url}`
      );
    }
  } catch (e) {
    console.log(
      `Could not create issue for ${milestone.title} for ${group.name}`
    );
    console.log(e);
  }
}

async function maybeCreatePlanningIssues() {
  const nextMilestone = await getMilestoneForDate(getDateOneMonthFromNow());
  if (!nextMilestone) {
    console.warn("Could not determine next milestone");
    return;
  }

  const issuesForNextMilestone = await getIssuesForMilestone(nextMilestone);

  return Promise.all(
    GROUPS.map((group) => {
      if (
        shouldCreatePlanningIssues(issuesForNextMilestone, group, nextMilestone)
      ) {
        return createPlanningIssueForGroup(group, nextMilestone);
      }
    })
  );
}

async function main() {
  await maybeCreatePlanningIssues();
}

main().catch((e) => {
  console.log("ERROR", e);
  process.exit(1);
});
